﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace superProjekt
{
    class Program
    {
        static void Main(string[] args)
        {   ///Title
            Console.BackgroundColor = ConsoleColor.Magenta;
            Console.Clear();
            Console.WriteLine("..............................................................................................................................\r");
            Console.WriteLine("..................................................AAAAAAAAAAA.................................................................\r");
            Console.WriteLine("...............................................A.AAAAAA#######................................................................\r");
            Console.WriteLine("...............................................AAAAA##########...........######...............................................\r");
            Console.WriteLine(".................................................##|O|##|O|###.........#####..................................................\r");
            Console.WriteLine(".................................................##NNNNNNNNN##.........###....................................................\r");
            Console.WriteLine(".................................................##|_______|##........###.....................................................\r");
            Console.WriteLine("..................................................####NNNN###........###......................................................\r");
            Console.WriteLine(".....................................................##NN...........###.......................................................\r");
            Console.WriteLine("............................................#########################.........................................................\r");
            Console.WriteLine("...........................................#########################..........................................................\r");
            Console.WriteLine("..........................................###//////##########.................................................................\r");
            Console.WriteLine(".........................................###///////##########.................................................................\r");
            Console.WriteLine("........................................###.///////##########.................................................................\r");
            Console.WriteLine("....................................#####...///////##########.................................................................\r");
            Console.WriteLine("...................................#####...////////##########.................................................................\r");
            Console.WriteLine("...........................................////////##########.................................................................\r");
            Console.WriteLine("........................................../////////#############..............................................................\r");
            Console.WriteLine("..........................................////////####///..######.............................................................\r");
            Console.WriteLine("........................................./////////####///....#####............................................................\r");
            Console.WriteLine("........................................./////////####///......####..................................m........................\r");
            Console.WriteLine("........................................//////////####///.......####..................................mm......................\r");
            Console.WriteLine("........................................//////////####//........####............mm...m.................mm.....................\r");
            Console.WriteLine(".......................................///////////####//........####............mmmmmm..mmmmmmmmmmmm..mm......................\r");
            Console.WriteLine(".......................................///////////####/.........####........\\.mmOmmmmmmmmmmmmmmmmmmmmm.......................\r");
            Console.WriteLine("......................................////////////####/.........####..........@mmm.mmmmmmmmmmmmmmmmm.m........................\r");
            Console.WriteLine("..................................................####..........###...........\\....mmmm........mm.mm.........................\r");
            Console.WriteLine("................................................#####...........######..............m.m..........m..m.........................\r");
            Console.WriteLine("...............................................####.................................m.m.........m..m..........................\r");
            Console.WriteLine("..............................................................................................................................\r");
            Console.WriteLine("..............................................................................................................................\r");
            Console.WriteLine(".########..#######..######...#######...##..##........##..##..##........##..#######..##........##..#######....@@....@@....@@...\r");
            Console.WriteLine(".########..#######..##...##..#######...##..####......##..##..####....####..##.......####......##..##........@@@@..@@@@..@@@@..\r");
            Console.WriteLine("....##.....##.......##...##..##........##..##.##.....##..##..##.##..##.##..##.......##.##.....##..##........@@@@..@@@@..@@@@..\r");
            Console.WriteLine("....##.....#######..######...#######...##..##..##....##..##..##..####..##..#######..##..##....##..#######...@@@@..@@@@..@@@@..\r");
            Console.WriteLine("....##.....#######..######...#######...##..##...##...##..##..##...##...##..#######..##...##...##..#######....@@....@@....@@...\r");
            Console.WriteLine("....##.....##.......##..##...##........##..##....##..##..##..##........##..##.......##....##..##..##..........................\r");
            Console.WriteLine("....##.....#######..##....##.#######...##..##......####..##..##........##..#######..##......####..#######....@@....@@....@@...\r");
            Console.WriteLine("....##.....#######..##....##.#######...##..##........##..##..##........##..#######..##........##..#######....@@....@@....@@...\r");
            ///Muutujad
            Console.WriteLine("--------------------------------");
            Console.Write("Kes sa oled? ");
            string name = Console.ReadLine();
            switch (name)
            {
                case "Peeter":
                    Console.WriteLine("Vägev!");
                    break;
                case "Jaan":
                    Console.WriteLine("Ülituus");
                    break;
                default:
                    Console.WriteLine("Nojah, okei. Trillalllaaa....");
                    break;
            }
            Console.Write("Kust pärit? ");
            string location = Console.ReadLine();
            if (location == "Hiinast")
            {
                Console.WriteLine("Vau, ülituus!");
            }
            else
            {
                Console.WriteLine("Nojah, okei.");
            }
            Console.WriteLine($"Sina oled " + name + " " + location + "!");
            Console.WriteLine("--------------------------------");

            //Mängu nimi
            Console.WriteLine("........................................................................VV..VV..VV..VV........................................");
            Console.WriteLine(".[]..[].[].[]..[].[]....||||||.||||||.||||||.||||||.||||||....VV...VV...................VVVVVV..VV..VVVV......................");
            Console.WriteLine(".[].[]..[].[]..[].[]....||..||.||..||.||..||.||.....||..||....VV..VV....VVVVVV..VVVVVV..VV..VV..VV..VV..VV....................");
            Console.WriteLine(".[][]...[].[]..[].[].@@.||..||.||..||.||||||.||||||.||||||.@@.VVVV......VV..VV..VV..VV..VVVVVV..VV..VV..VV....................");
            Console.WriteLine(".[].[]..[]..[][]..[]....||||||.||||||.||..||.||.....||.||.....VV..VV....VVVVVV..VVVVVV..VV.VV...VV..VV..VV....................");
            Console.WriteLine(".[]..[].[]...[]...[]....||.....||..||.||||||.||||||.||..||....VV....VV..VV..VV..VV..VV..VV..VV..VV..VVVV......................");
            //Mängu juhend
            Console.WriteLine("Eepiline Kivi-Paber-Käärid Lahing!!!");
            Console.WriteLine("Mängi 10 raundi. Võidab see, kellel on mängu lõpuks rohkem punkte!!!");

            //Roundi ja skoori seadmine
            int round = 0;
            int sinuSkoor = 0;
            int vaenlaseSkoor = 0;

            while (round <= 10)
            {
                //Arvestus
                Console.WriteLine("--------------------------------");
                Console.WriteLine("Round: " + round);
                Console.WriteLine("--------------------------------");
                Console.WriteLine(sinuSkoor + ":" + vaenlaseSkoor);
                Console.WriteLine("--------------------------------");

                //Mängija input
                Console.WriteLine("Kivi, Paber või Käärid?");
                string mängijaInput = Console.ReadLine();
                int mängijaKäik;
                switch (mängijaInput)
                {
                    case "Kivi":
                        Console.WriteLine("........................");
                        Console.WriteLine(".......0000000**........");
                        Console.WriteLine("......0000#####**.......");
                        Console.WriteLine("......00#######**.......");
                        Console.WriteLine("......00#####****.......");
                        Console.WriteLine(".......*********........");
                        Console.WriteLine("........................");

                        mängijaKäik = 1;
                        break;
                    case "Paber":
                        Console.WriteLine("........................");
                        Console.WriteLine("..........///////////...");
                        Console.WriteLine(".........///////////....");
                        Console.WriteLine("........///////////.....");
                        Console.WriteLine(".......///////////......");
                        Console.WriteLine("......///////////.......");
                        Console.WriteLine("........................");

                        mängijaKäik = 2;
                        break;
                    case "Käärid":
                        Console.WriteLine("......n......n..........");
                        Console.WriteLine(".......n....n...........");
                        Console.WriteLine("........N..N............");
                        Console.WriteLine(".........><.............");
                        Console.WriteLine(".......nn..nn...........");
                        Console.WriteLine("......n.n..n.n..........");
                        Console.WriteLine(".......n....n...........");

                        mängijaKäik = 3;
                        break;
                    default:
                        continue;
                }

                //Vaenlase käik ja tulemuse arvestamine
                Random rnd = new Random();
                int vaenlane = rnd.Next(1, 4);
                switch (vaenlane)
                {
                    case 1:
                        Console.WriteLine("Vaenlane valis Kivi");
                        if (mängijaKäik == 3)
                        {
                            Console.WriteLine("Vaenlane võitis!");
                            vaenlaseSkoor += 1;
                        }
                        else if (mängijaKäik == 2)
                        {
                            Console.WriteLine("Sinu võit!");
                            sinuSkoor += 1;
                        }
                        else
                        {
                            Console.WriteLine("Viik!");
                        }
                        break;
                    case 2:
                        Console.WriteLine("Vaenlane valis Paber");
                        if (mängijaKäik == 1)
                        {
                            Console.WriteLine("Vaenlane võitis!");
                            vaenlaseSkoor += 1;
                        }
                        else if (mängijaKäik == 3)
                        {
                            Console.WriteLine("Sinu võit!");
                            sinuSkoor += 1;
                        }
                        else
                        {
                            Console.WriteLine("Viik!");
                        }
                        break;
                    case 3:
                        Console.WriteLine("Vaenlane valis Käärid");
                        if (mängijaKäik == 2)
                        {
                            Console.WriteLine("Vaenlane võitis!");
                            vaenlaseSkoor += 1;
                        }
                        else if (mängijaKäik == 1)
                        {
                            Console.WriteLine("Sinu võit!");
                            sinuSkoor += 1;
                        }
                        else
                        {
                            Console.WriteLine("Viik!");
                        }
                        break;
                }


                round += 1;
            }
            //Lõpptulemus
            Console.WriteLine("--------------------------------");
            Console.WriteLine("--------------------------------");

            if (sinuSkoor > vaenlaseSkoor)
            {
                Console.WriteLine("Jee, sa oled nii osav!");
            }
            else
            {
                Console.WriteLine("Möku, proovi uuesti!");
            }
            Console.WriteLine("--------------------------------");
            Console.WriteLine("--------------------------------");
        }
    }
}
